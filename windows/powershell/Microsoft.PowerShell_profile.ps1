oh-my-posh init pwsh --config "$env:POSH_THEMES_PATH/quick-term.omp.json" | Invoke-Expression

# Style default PowerShell Console
$shell = $Host.UI.RawUI

#$shell.WindowTitle= "PWSH"

$shell.BackgroundColor = "Black"
$shell.ForegroundColor = "White"

# Load custom theme for Windows Terminal
Import-Module posh-git
Import-Module PSReadLine
Import-Module MSOnline
Set-PSReadLineOption -PredictionSource History -HistorySearchCursorMovesToEnd
#Set-PSReadlineKeyHandler -Key Tab -Function MenuComplet
Set-PSReadLineKeyHandler -Chord UpArrow -Function HistorySearchBackward
#Set-PSReadLineOption -HistorySearchCursorMovesToEnd
#Set-PSReadLineKeyHandler -Chord Tab -Function HistorySearchBackward

# Load scripts from the following locations
#$env:Path += ";D:\SysAdmin\scripts\PowerShellBasics"
#$env:Path += ";D:\SysAdmin\scripts\Connectors"
#$env:Path += ";D:\SysAdmin\scripts\Office365"

# Lazy way to use scripts as module
#Set-Alias ConnectTo-SharePointAdmin ConnectTo-SharePointAdmin.ps1
#Set-Alias ConnectTo-EXO ConnectTo-EXO.ps1
#Set-Alias Get-MFAStatus MFAStatus.ps1
#Set-Alias Get-MailboxSizeReport MailboxSizeReport.ps1
#Set-Alias Get-OneDriveSizeReport OneDriveSizeReport.ps1

# Create aliases for frequently used commands
Set-Alias im Import-Module
Set-Alias tn Test-NetConnection

#Invoke-Expression (&starship init powershell)

# Import the Chocolatey Profile that contains the necessary code to enable
# tab-completions to function for `choco`.
# Be aware that if you are missing these lines from your profile, tab completion
# for `choco` will not function.
# See https://ch0.co/tab-completion for details.
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
  Import-Module "$ChocolateyProfile"
}

## Funkcija za dodajanje aliasa LL
function Get-ChildItemUnix {
    Get-ChildItem $Args[0] |
        Format-Table Mode, @{N='Owner';E={(Get-Acl $_.FullName).Owner}}, Length, LastWriteTime, @{N='Name';E={if($_.Target) {$_.Name+' -> '+$_.Target} else {$_.Name}}}
}
New-Alias ll Get-ChildItemUnix

# This function will prompt for authentication and connect to Exchange on-premises
# Use -URL to specify a URL, or set the default URL to your most preferred server.
Function Connect-Exchange {

    param(
        [Parameter( Mandatory=$false)]
        [string]$URL="exchange-app1.vzz.si"
    )

    $Credentials = Get-Credential -Message "Enter your Exchange admin credentials"

    $ExOPSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://$URL/PowerShell/ -Authentication Kerberos -Credential $Credentials

    Import-PSSession $ExOPSession

}